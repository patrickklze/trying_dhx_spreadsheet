import React, { PureComponent } from "react";
import smoothscroll from "smoothscroll-polyfill";
import logo from './logo.svg';
import './App.css';
import "dhx-suite/codebase/suite.min.css";
import SpreadsheetComponent from './spreadsheet/Spreadsheet';

class App extends PureComponent {
  constructor(props) {
		super(props);
		smoothscroll.polyfill();
		this.state = {
		};
	}
  render() {
      return (
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Edit <code>src/App.js</code> and save to reload.
            </p>
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
            <span style={{textAlign:"left"}}>
          <SpreadsheetComponent />
          </span>
          </header>
        </div>
      );
  }
}

export default App;
